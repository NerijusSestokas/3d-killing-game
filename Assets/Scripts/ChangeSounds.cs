﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeSounds : MonoBehaviour
{
    public Slider volumeSlid;

    void Start()
    {
        volumeSlid.value = PlayerPrefs.GetFloat("SoundVolume");
    }

    public void ChangeSound(float value)
    {
        PlayerPrefs.SetFloat("SoundVolume", value);
        volumeSlid.value = value;
    }
}
