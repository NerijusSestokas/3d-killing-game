﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    public GameObject ChasePlayer;
    public float Speed = 3.5f;
    public float MinimumDistace = 2.5f;
    public int HitDamage = 10;
    public float TimeC = 2.0f;          // Time to cool down after swing
    public int StartHealth = 50;

    private int CurrentHealth;
    private float TimeL = 0.0f;
    private bool isAlive;
    private Animator anim;

    void Start()
    {
        CurrentHealth = StartHealth;
        if (GetComponentInChildren<Animator>())
        {
            anim = GetComponentInChildren<Animator>();
        }
        else
        {
            Debug.LogError("The character needs a animator.");
        }
        isAlive = true;
        TimeL = TimeC;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void LateUpdate()
    {
        TimeL -= Time.deltaTime;
    }

    void OnTriggerStay(Collider other)
    {
        // Chase Main Character
        if (other.name == ChasePlayer.name && isAlive)
        {
            // Chase Player
            if (Vector3.Distance(transform.position, ChasePlayer.transform.position) > MinimumDistace && TimeL <= 0)
            {
                    transform.LookAt(ChasePlayer.transform);
                    transform.position += transform.forward * Speed * Time.deltaTime;
                    anim.SetInteger("Anim", 2);
            }
            // Hit Player
            else if (Vector3.Distance(transform.position, ChasePlayer.transform.position) < MinimumDistace)
            {
                anim.SetInteger("Anim", 0);
                if (TimeL <= 0)
                {
                    float rand = Random.value;
                    if (rand < .50f)
                        anim.Play("Fist_Left");
                    else if (rand > .50f)
                        anim.Play("Fist_Right");
                    GameObject.Find("Main Camera").SendMessage("TakeDamage", HitDamage);
                    TimeL = TimeC;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Stop from Chasing Players
        if (other.name == ChasePlayer.name && isAlive)
        {
            anim.SetInteger("Anim", 0);
        }
    }

    public void TakeDamage(int amount)
    {
        // Reduce the current health by the damage amount.
        CurrentHealth -= amount;
        if (CurrentHealth <= 0 && isAlive)
        {
            ChasePlayer.SendMessage("EnemyIsKilled", true);
            EnemyDeath();
            isAlive = false;
        }
    }

    public void EnemyDeath()
    {
        isAlive = false;
        anim.Play("Death");
    }
}
