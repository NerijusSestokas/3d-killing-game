﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealtBar : MonoBehaviour {

    public int startingHealth = 100;                            // The amount of health the player starts the game with.
    public int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                                 // Reference to the UI's health bar.


    void Start()
    {
        // Set the initial health of the player.
        currentHealth = startingHealth;
        healthSlider.value = 1;
    }


    void Update()
    {
    }


    public void TakeDamage(int amount)
    {
        // Reduce the current health by the damage amount.
        currentHealth -= amount;

        // Set the health bar's value to the current health.
        float cur = currentHealth;
        healthSlider.value = cur / 100;

        // If the player has lost all it's health
        if (currentHealth <= 0)
        {   
            Application.LoadLevel("GameOver");
        }
    }

    public void RegenHealth(int amount)
    {
        currentHealth += amount;
        if(currentHealth > startingHealth)
        {
            currentHealth = startingHealth;
        }
        float cur = currentHealth;
        healthSlider.value = cur / 100;
    }

}