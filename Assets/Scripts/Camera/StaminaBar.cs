﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StaminaBar : MonoBehaviour
{

    public int startingStamina = 100;                            // The amount of stamina the player starts the game with.
    public int currentStamina;                                   // The current stamina the player has.
    public Slider StaminaSlider;                                 // Reference to the UI's stamina bar.
    public int gainStamina = 5;                                  // The amount of stamina gain per time if stamina is not full.
    public float time = 2.0f;                                    // The amount of time to gain stamina.
    private float gTime = 0.0f;


    void Start()
    {
        // Set the initial health of the player.
        currentStamina = startingStamina;
        StaminaSlider.value = 1;
    }


    void Update()
    {
        if (gTime >= time)
        {
            gTime = 0.0f;
        }
        gTime += Time.deltaTime;
    }


    public void TakeStamina(int amount)
    {
        if (gTime >= time && currentStamina > 0)
        {
            // Reduce the current stamina by the damage amount.
            currentStamina -= amount;
        }

        // Set the stamina bar's value to the current stamina.
        float cur = currentStamina;
        StaminaSlider.value = cur / 100;

    }

    public void GiveStamina()
    {
        if (gTime >= time)
        {
            if (currentStamina < startingStamina)
            {
                currentStamina += gainStamina;
                float cur = currentStamina;
                StaminaSlider.value = cur / 100;
            }
        }
    }

    public bool isEmpty()
    {
        if(currentStamina <= 0)
        {
            return true;
        }
        return false;
        

    }

    public bool CanRun()
    {
        if(currentStamina > 10)
        {
            return true;
        }
        return false;
    }
}