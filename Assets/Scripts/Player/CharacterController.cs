﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour 
{
    public float lockY = 0.07f;
    public float inputDelay = 0.1f;
    public float walkSpeed = 3;
    public float runSpeed = 6;
    public float rotateVel = 80;
    public int reduceStamina = 5;
    public AudioClip FootSteps;
    public AudioClip RunSound;


    Animator anim;
    Quaternion targetRotation;
    Rigidbody rBody;
    float forwardInput, turnInput;
    bool runBut = false;
    GameObject mainCamera;
    private float Sound;
    private bool menu = false;
    private AudioSource audioSource;
    
    public Quaternion TargetRotation
    {
        get { return targetRotation; }
    }

    void Start()
    {
        Sound = PlayerPrefs.GetFloat("SoundVolume");
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = Sound;
        targetRotation = transform.rotation;

        if (GetComponent<Rigidbody>())
        {
            rBody = GetComponent<Rigidbody>();
        }
        else
        {
            Debug.LogError("The character needs a rigidbody.");
        }
        forwardInput = turnInput = 0;

        if(GetComponentInChildren<Animator>())
        {
            anim = GetComponentInChildren<Animator>();
        }
        else
        {
            Debug.LogError("The character needs a animator.");
        }

        if(GameObject.Find("Main Camera"))
        {
            mainCamera = GameObject.Find("Main Camera");
        }
        else
        {
            Debug.LogError("Can't find camera with name ''Main Camera''");
        }
    }

    void GetInput()
    {
        forwardInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
        runBut = Input.GetKey(KeyCode.LeftShift);
        menu = Input.GetKey(KeyCode.M);
        if(menu)
        {
            Application.LoadLevel(0);
        }
        
    }

    void Update()
    {
        GetInput();
        Turn();
    }

    void FixedUpdate()
    {
        Run();
    }

    void Run()
    {
        var forward = transform.forward;
        forward.y = 0f;
        if (Mathf.Abs(forwardInput) > inputDelay && !runBut)
        {
            // move
            rBody.velocity = forward * forwardInput * walkSpeed;
            anim.SetInteger("Anim", 1);
            GameObject.Find("Main Camera").SendMessage("GiveStamina");
        }
        else if (Mathf.Abs(forwardInput) > inputDelay && runBut && !mainCamera.GetComponent<StaminaBar>().isEmpty())
        {
            // run
            rBody.velocity = forward * forwardInput * runSpeed;
            anim.SetInteger("Anim", 2);
            GameObject.Find("Main Camera").SendMessage("TakeStamina", reduceStamina);
            
        }
        else if (Mathf.Abs(forwardInput) > inputDelay && runBut && mainCamera.GetComponent<StaminaBar>().isEmpty())
        {
            // move
            rBody.velocity = forward * forwardInput * walkSpeed;
            anim.SetInteger("Anim", 1);
            GameObject.Find("Main Camera").SendMessage("GiveStamina");
            audioSource.clip = FootSteps;
            audioSource.Play();
        }
        else
        {
            // don't move
            rBody.velocity = Vector3.zero;
            anim.SetInteger("Anim", 0);
            GameObject.Find("Main Camera").SendMessage("GiveStamina");
        }
    }

    void Turn()
    {
        if (Mathf.Abs(turnInput) > inputDelay)
        {
            targetRotation *= Quaternion.AngleAxis(rotateVel * turnInput * Time.deltaTime, Vector3.up);
        }
        transform.rotation = targetRotation;        
    }

    void LateUpdate()
    {
        this.transform.position = new Vector3(this.transform.position.x, lockY, this.transform.position.z);
        if (anim.GetNextAnimatorStateInfo(0).IsName("Walk"))
        {
            audioSource.clip = FootSteps;
            audioSource.Play();
        }
        if(anim.GetNextAnimatorStateInfo(0).IsName("Run"))
        {
            audioSource.clip = RunSound;
            audioSource.Play();
        }
        if(anim.GetNextAnimatorStateInfo(0).IsName("Idle"))
        {
            audioSource.Stop();
        }
    }
}
