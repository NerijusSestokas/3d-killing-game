﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour
{
    private GameObject bat;
    private bool pick = false;

    // Use this for initialization
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Bat_Hand"))
        {
            bat = GameObject.FindGameObjectWithTag("Bat_Hand");
            bat.SetActive(false);
        }
        else
            Debug.Log("Cannot find Game Object with tag 'Bat_Hand'");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Pick(GameObject obj)
    {
        pick = Input.GetKey(KeyCode.E);
        if (obj.tag == "Weapon_Bat")
        {
            if (pick)
            {
                bat.SetActive(true);
                GameObject.Find("Main").SendMessage("WeaponUsing");
                Destroy(obj);
            }
        }
    }
}
