﻿using UnityEngine;
using System.Collections;

public class CharacterFight : MonoBehaviour
{
    public int killedEnemys = 0;
    public int allEnemys;
    public int easyKill = 1;

    private bool punch = false;
    private Animator anim;
    private float gTime = 0.0f;
    private bool IsWeapon = false;
    private GameObject ene = null;
    private bool isTriggered = false;
    private float timeL = 1f;             // Cool down time after swing
    private float timeC = 0f;
    private bool regen = false;           

    void Start()
    {
        GameObject[] enemys;
        enemys = GameObject.FindGameObjectsWithTag("Enemy");
        allEnemys = enemys.Length;
        if (GetComponentInChildren<Animator>())
            anim = GetComponentInChildren<Animator>();
        else
            Debug.LogError("The character needs a animator.");
    }

    void Update()
    {
    }

    void LateUpdate()
    {
        timeC -= Time.deltaTime;
        if (ene != null && isTriggered && timeC <= 0)
        {
            if (ene.tag == "Enemy")
            {
                punch = Input.GetKey(KeyCode.F);
                string enemyName = ene.name;
                if (punch)
                {
                    timeC = timeL;
                    StartCoroutine(Fight(enemyName));
                }
            }
        }
    }

    void OnTriggerStay(Collider obje)
    {
        if (obje.tag == "Enemy")
        {
            isTriggered = true;
            ene = obje.gameObject;
        }
        if(obje.tag == "HealthBox")
        {
            regen = Input.GetKey(KeyCode.E);
            if(regen)
            {
                GameObject.Find("Main Camera").SendMessage("RegenHealth", 20);
                Destroy(obje.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider obje)
    {
        if (obje.tag == "Enemy")
        {
            if (obje.name == ene.name)
            {
                isTriggered = false;
                ene = null;
            }
        }
    }

    void TakeEnemyHealth(int amount, string enemyName)
    {
        GameObject.Find(enemyName).SendMessage("TakeDamage", amount);
    }

    public void EnemyIsKilled(bool tmp)
    {
        if (tmp == true)
        {
            killedEnemys++;
            GameObject.Find("ScoreDisp").SendMessage("AddScore", easyKill);
        }
        GameWin();
    }

    void GameWin()
    {
        if (killedEnemys >= allEnemys)
        {
            StartCoroutine(End());
        }
    }

    IEnumerator Fight(string enemyName)
    {
        yield return new WaitForSeconds(1f);
        float rand = Random.value;
        if (IsWeapon)
        {
            anim.Play("Swing");
            TakeEnemyHealth(20, enemyName);
        }
        else if (rand < .50f && !IsWeapon)
        {
            anim.Play("Fist_Left");
            TakeEnemyHealth(10, enemyName);
        }
        else if (rand > .50f && !IsWeapon)
        {
            anim.Play("Fist_Right");
            TakeEnemyHealth(10, enemyName);
        }
    }

    IEnumerator End()
    {
        yield return new WaitForSeconds(4f);
        if (Application.loadedLevelName == "Scene01")
        {
            Application.LoadLevel(6);
        }
        else
        {
            Application.LoadLevel(4);
        }
    }

    public void WeaponUsing()
    {
        IsWeapon = true;
    }
}
