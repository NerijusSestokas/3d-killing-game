﻿using UnityEngine;
using System.Collections;

public class CanPickUp : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerStay(Collider obje)
    {
        if (obje.tag == "Player")
        {
            GameObject.Find("Main").SendMessage("Pick", this.gameObject);
        }
    }
}
